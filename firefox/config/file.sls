# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as firefox with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('firefox-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_firefox', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

firefox-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

firefox-config-file-bin-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/bin
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - firefox-config-file-user-{{ name }}-present

{%- set firefox_user = user.get('firefox', {}) %}
{%- set profiles = firefox_user.get('profiles', []) %}
{%- for profile in profiles %}
firefox-config-file-profile-{{ profile }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-firefox
    - source: {{ files_switch([
                  name ~ '-launch-firefox-profile.sh.tmpl',
                  'launch-firefox-profile.sh.tmpl'],
                lookup='firefox-config-file-profile-' ~ profile ~ '-' ~ name ~ '-managed',
                )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        profile: {{ profile }}
    - require:
      - firefox-config-file-bin-dir-{{ name }}-managed
{% endfor %}
{% endif %}
{% endfor %}
{% endif %}
