# frozen_string_literal: true

profiles = %w[default work]
profiles.each do |profile|
  control "firefox-config-clean-profile-#{profile}-auser-absent" do
    title 'should not exist'

    describe file("/home/auser/bin/#{profile}-firefox") do
      it { should_not exist }
    end
  end
end
