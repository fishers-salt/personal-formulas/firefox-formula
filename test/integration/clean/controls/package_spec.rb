# frozen_string_literal: true

packages = %w[firefox firefox-i18n-en-gb]

packages.each do |pkg|
  control "firefox-package-clean-#{pkg}-pkg-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
