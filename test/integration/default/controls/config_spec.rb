# frozen_string_literal: true

control 'firefox-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'firefox-config-file-bin-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/bin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

profiles = %w[default work]
profiles.each do |profile|
  control "firefox-config-file-profile-#{profile}-auser-managed" do
    title 'should exist'

    describe file("/home/auser/bin/#{profile}-firefox") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0755' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include("firefox -P #{profile} -no-remote") }
    end
  end
end
