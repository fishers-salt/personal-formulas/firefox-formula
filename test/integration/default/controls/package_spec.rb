# frozen_string_literal: true

packages = %w[firefox firefox-i18n-en-gb]

packages.each do |pkg|
  control "firefox-package-install-#{pkg}-pkg-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
